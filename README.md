# Ids721 Week 4 Project

### Steps:
#### Setting Docker:
1. Download Docker Desktop for Mac
2. Install Docker Desktop
3. Run Docker Desktop
4. Verify Installation. Run `docker --version` to check that Docker has been installed successfully.
5. Run `docker run hello-world` to download a test image and run it in a container, which verifies that Docker is working correctly on your system.

#### Creating Simple Rust Actix Web App
1. `cargo new week4_project`
2. `cd week4_project`
3. Add `actix-web` to your `Cargo.toml` under `[dependencies]`
4. Replace the content of `src/main.rs` with a simple Actix web server
 ```
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, Actix!")
}
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
 ```
5. Create a Dockerfile
 ```
# Use the official Rust image as a builder
FROM rust:1.56 as builder

# Create a new empty shell project
RUN USER=root cargo new --bin rust_actix_web_app
WORKDIR /rust_actix_web_app

# Copy our manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# Build only the dependencies to cache them
RUN cargo build --release
RUN rm src/*.rs

# Copy the source and build the application
COPY ./src ./src
RUN rm ./target/release/deps/rust_actix_web_app*
RUN cargo build --release

# Use the Debian Buster slim image for the runtime
FROM debian:buster-slim
ARG APP=/usr/src/app

# Copy the binary from the builder stage
COPY --from=builder /rust_actix_web_app/target/release/rust_actix_web_app ${APP}/rust_actix_web_app

# Set the working directory and the binary as the entrypoint
WORKDIR ${APP}
ENTRYPOINT ["./rust_actix_web_app"]

# Expose the application on port 8080
EXPOSE 8080

 ```
 6. `cargo build`
 7. Build the Docker Image
 `docker build -t rust_actix_web_app .
`
8. Run the Container Locally
`docker run -p 8080:8080 rust_actix_web_app`

9. Open Localhost:8080
![screenshot](image/screen.png)
