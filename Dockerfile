FROM rust:1.75 as builder

RUN USER=root cargo new mp4
WORKDIR /mp4

COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
RUN rm src/*.rs

COPY ./src ./src

RUN rm ./target/release/deps/mp4*
RUN cargo build --release

FROM debian:bookworm-slim
COPY --from=builder /mini4/target/release/mp4 .
ENV ROCKET_ADDRESS=0.0.0.0
CMD ["./mp4"]

